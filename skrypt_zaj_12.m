%{

    Kamila Pajak
    Engineering physics

    Oscillations

    Tested on Matlab R2019b

%}

%{

    EXPLANATION

All constants are in SI units (m, kg, m/s^2)

Applied formulas below

%}

clear

wireRadius = 0.002;
coilRadius = 2 / 39.327;
coilAmount = 2 : 1 : 50; 
M = 0.01; 
shearModulus = 77.2e9; 

Tau = 2; 
Alpha = 9.81;
Beta = 1 / (2 / Tau);
Omega = 50 : 0.1 : 400;

for j = 1 : size(coilAmount, 2)
  k = shearModulus * wireRadius ^ 4 / (4 * coilAmount(j) * coilRadius ^ 3);
  % obtain sprint constant
  Omega0 = sqrt(k / M);
  % resonance frequency
  
  for i = 1 : size(Omega, 2)
    Amplitude(i, j) = Alpha / sqrt((Omega0 ^ 2-Omega(i) ^ 2) ^ 2 + 4 * Beta ^ 2 * Omega(i) ^ 2);
  end
  
  [maximumAmplitude(j), index] = max(Amplitude(:, j));
  % finding maximum values
  resonanceFrequency(j) = Omega(index) / (2 * pi);
  
end

% two charts
subplot(2, 1, 1);
% subplot(arg, arg, arg) divides into a grid, just like matplotlib does
plot(Omega / (2 * pi), Amplitude);
title('Resonance amplitude level and number of coils dependence')
xlim([8, 80])
xlabel('Frequency [Hz]');
ylabel('Amplitude [m]');
grid on; 

subplot(2, 1, 2);
plot(coilAmount, maximumAmplitude);
title('Maximum amplitude and number of coils dependence')
xlim([2, 50])
xlabel('Number of coils');
ylabel('Maximum amplitude values [m]');
grid on;
saveas(gcf,'resonance.pdf'); 
% gcf refers to current figure

Filename=fopen('resonance_analysis_results.dat', 'w');
fprintf(Filename,'wireRadius = %.3f[m]; coilRadius = %.3f[m]; M = %.2f[kg]; Tau = %.1f[s]; Alpha = %.2f[m / s^2]; shearModulus = %.2d[Pa]\n\n', wireRadius, coilRadius, M, Tau, Alpha, shearModulus);
% saving to file

for i = 1 : size(coilAmount,2)
    
	fprintf(Filename, 'coil amount = %.0f; maximum amplitude = %.3f [m]; frequency = %.3f [Hz] \n', coilAmount(i), maximumAmplitude(i), resonanceFrequency(i));
    % loop through file to append data

end

fclose(Filename);
